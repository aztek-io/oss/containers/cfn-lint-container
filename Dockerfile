FROM alpine

LABEL maintainer="robert@scriptmyjob.com"

RUN apk update

RUN apk add py3-pip

RUN pip3 install cfn-lint

RUN cfn-lint --version | \
        grep -Eow '([0-9]+\.){2}[0-9]+' | \
        head -n 1 > /version.out

