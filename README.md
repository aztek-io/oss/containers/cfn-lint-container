# README

## INFO

This is an automated build that will kick off at midnight (CDT) until one of the following:
* The heat death of the universe (Can entropy be reversed?)
* I stop paying my GitLab bill (whichever comes first - immortality joke).

```¯\_(ツ)_/¯```

This was created since there is not an officail git docker image.

## HUB REPO

Here is the hub.docker.com repo:

https://hub.docker.com/r/aztek/cfn-lint/
